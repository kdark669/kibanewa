<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class Usercontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user=User::paginate(3);
        return view('back.user.add',['rows'=>$user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $user=new User();
        $user->user_validate($request);
        $img=$request->image;
        if(isset($img)){
            $dest_path="images/user/";
            $file_name=uniqid()."-".$img->getClientOriginalName();
            $img->move($dest_path,$file_name);
            $user->image=$dest_path.$file_name;
        }
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=$request->password;

        $user->save();


        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $result=User::find($id);
        $user= User::paginate(3);
        return view('back.user.edit',['result'=>$result,'rows'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user=User::find($id);
        $img=$request->image;
        if(isset($img)){
            $pic=$request->pic;
            if(isset($pic)){
                unlink($pic);
            }
            $dest_path="images/user/";
            $file_name=uniqid()."-".$img->getClientOriginalName();
            $img->move($dest_path,$file_name);
            $user->image=$dest_path.$file_name;
        }
        $user->name=$request->name;
        $user->email=$request->email;

        $user->save();
        return redirect('user');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user=User::find($id);
        $img=$user->image;
        if(isset($img)){
            unlink($img);
        }
        $user->delete();
        return redirect('user');
    }
}
