<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;
use auth;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $about=About::find(1);
        return view('back.Dabout.index',['result'=>$about]);
    }

    public function update(Request $request, $id)
    {
        //
        $about=About::find($id);
        $img=$request->image;
        if(isset($img)){
            $pic=$request->pic;
                if(isset($pic)){
                    unlink($pic);
                }
            $dest_path="images/about/";
            $file_name=$img->getClientOriginalName();
            $img->move($dest_path,$file_name);

            $about->image=$dest_path.$file_name;
        }
        $about->heading=$request->heading;
        $about->title=$request->title;
        $about->description=$request->description;
        $about->caption=$request->caption;
        $about->keywords=$request->keywords;
        $about->metaTag=$request->metaTag;
        $about->metaDesc=$request->metaDesc;
        $about->UID=Auth::user()->id;

        $about->save();

        return redirect('Dabout');
    }
}
