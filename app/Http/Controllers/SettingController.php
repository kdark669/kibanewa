<?php

namespace App\Http\Controllers;
use App\Setting;
use auth;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $setting=Setting::find(1);
        return view('back.setting.index',['result'=>$setting]);
    }


    public function update(Request $request, $id)
    {
        //
        $setting=Setting::find($id);

        $img=$request->logo;
        if(isset($img)){
            $pic=$request->pic;
            if(isset($pic)){
                unlink($pic);
            }
            $dest_path="images/";
            $file_name=$img->getClientOriginalName();
            $img->move($dest_path,$file_name);
            $setting->logo=$dest_path.$file_name;
        }
        $setting->company=$request->company;
        $setting->description=$request->description;
        $setting->address=$request->address;
        $setting->contact=$request->contact;
        $setting->email=$request->email;
        $setting->facebook=$request->facebook;
        $setting->caption=$request->caption;
        $setting->keyword=$request->keywords;
        $setting->metaTag=$request->metaTag;
        $setting->metaDesc=$request->metaDesc;
        $setting->UID=Auth::user()->id;

        $setting->save();

        return redirect('setting');

    }

}
