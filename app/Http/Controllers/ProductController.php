<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $product= Product::paginate(4);
        return view('back.Dproduct.add',['rows'=>$product]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $product= new Product();
        $img=$request->image;
        if(isset($img))
        {
            $dest_path="images/product/";
            $file_name=uniqid()."-".$img->getClientOriginalName();
            $img->move($dest_path,$file_name);
            $product->image=$dest_path.$file_name;
        }
        $product->title=$request->title;
        $product->heading=$request->heading;
        $product->description=$request->description;
        $product->UID=Auth::user()->id;

        $product->save();
        return redirect('Dproduct');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $result=Product::find($id);
        $product=Product::paginate(4);
        return view('back.Dproduct.edit', ['result'=>$result, 'rows'=>$product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $product=Product::find($id);
        $img=$request->image;
        if(isset($img)){
            $pic=$request->pic;
            if(isset($pic)){
                unlink($pic);
            }
            $dest_path="images/product/";
            $file_name=uniqid()."-".$img->getClientOriginalName();
            $img->move($dest_path,$file_name);

            // send to database
            $product->image=$dest_path.$file_name;
        }
        $product->title=$request->title;
        $product->heading=$request->heading;
        $product->description=$request->description;

        $product->save();
        return redirect('Dproduct');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $product=Product::find($id);
        $img=$product->image;
        if(isset($img)){
            unlink($img);
        }
        $product->delete();
        return redirect('Dproduct');
    }
}
