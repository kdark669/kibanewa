<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Setting;
use App\About;
use App\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $setting=Setting::find(1);
        return view('front.home',['result'=>$setting]);
    }
    public function about()
    {
        $about=About::find(1);
        return view('front.page.about',['about'=>$about]);
    }
    public function product()
    {
        $product=Product::all();
        return view('front.page.product',['product'=>$product]);
    }
}
