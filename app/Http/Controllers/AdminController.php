<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use auth;
use User;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
		$this->middleware('auth');
    }


    public function admin()
    {
        //
        return view('back.admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        //
        return redirect('admin');
    }
    public function logout()
    {
        //
        Auth::logout();
        return redirect('admin');
    }
}
