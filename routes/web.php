<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// frontend(front)
Route::get('/', function () {
    return view('front.home');
});
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about','HomeController@about');
Route::get('/product','HomeController@product');
Route::get('/store','HomeController@store');

// backend(back)
// admin
Route::get('/admin','AdminController@admin');
Route::get('/login','AdminController@login');
Route::get('/logout','AdminController@logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//setting
Route::get('/setting','SettingController@index');
Route::post('/setting/update/{id}', 'SettingController@update');

// user
Route::get('/user','UserController@index')->name('index');
Route::post('/user/add','UserController@store')->name('store');
Route::get('/user/edit/{id}','UserController@edit')->name('edit');
Route::post('/user/update/{id}','UserController@update')->name('update');
Route::get('/user/delete/{id}','Usercontroller@destroy');

//about
Route::get('/Dabout','AboutController@index');
Route::post('/Dabout/update/{id}','AboutController@update');

//Store
Route::get('/Dstore','StoreController@index');
Route::post('/Dstore/add','StoreController@store');

// products
Route::get('/Dproduct', 'ProductController@index');
Route::post('/Dproduct/add','ProductController@store');
Route::get('/Dproduct/edit/{id}','ProductController@edit');
Route::post('/Dproduct/update/{id}','ProductController@update');
Route::get('/Dproduct/delete/{id}','ProductController@destroy');





