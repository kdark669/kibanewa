@extends('back.layout.master')
@section('content')
<section id="main-content">
    <section class="wrapper">
        <form method="POST" action="{{url('Dabout/update')}}/{{$result->id}}" enctype="multipart/form-data">
            @csrf
            {{-- overview start --}}
            <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                        <div class="row">
                            <div class="col-md-6">
                            <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | About</li>
                            </div>
                        </div>
                        </ol>
                    </div>
            </div>
            <br>
            <br>
            {{-- overstart end --}}

            {{-- ckeditor//description start --}}
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-wrapper well">
                        <div class="form-group">
                            <!-- logo -->
                            <center>
                                <img src="{{($result->image)}}" class="img img-responsive" alt="images">
                            </center>
                            <br>
                            <input type="hidden" name="pic" value="{{$result->image}}">
                            <!-- end of logo -->
                            <!-- button -->
                            <input type="file" name="image" class="file">
                            <div class="input-group col-xs-12">
                                <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                                <span class="input-group-btn">
                                    <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i> Browse</button>
                                </span>
                            </div>
                            <!-- End of button -->
                            <br>
                            <!--Adress -->
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <br>
                                    <label for="title">Heading:</label>
                                    <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="heading" value="{{($result->heading)}}">
                                    <br>
                                    <label for="title">Title:</label>
                                    <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="title" value="{{($result->title)}}">
                                    <br>
                                    <label for="comment">Description:</label>
                                    <textarea name="description" class="ckeditor">{{($result->description)}}</textarea>
                                    <br>
                                </div>

                                <!-- right -->
                                <div class="col-lg-6">
                                        <h2>SEO</h2>
                                        <div class="form-wrapper well">
                                            <div class="form-group">
                                                <!-- Caption -->
                                                <div class="row">
                                                    <div class="col-md-3"><label for="text">Caption:</label></div>
                                                    <div class="col-md-9"><input type="text" class="form-control" id="text" placeholder="KITES NEPAL " name="caption" value="{{($result->caption)}}"></div>
                                                </div>
                                                <!-- End of caption -->
                                                <br>
                                                <!--keywords -->
                                                <div class="row">
                                                    <div class="col-md-3"><label for="text">keywords:</label></div>
                                                    <div class="col-md-9"><input type="text" class="form-control" id="text" placeholder="Gapali -4, Suryavinyak" name="keywords" value="{{($result->keywords)}}"></div>
                                                </div>
                                                <!-- end of keywords -->
                                                <br>
                                                <!-- metatag -->
                                                <div class="row">
                                                    <div class="col-md-3"><label for="text">Metatag:</label></div>
                                                    <div class="col-md-9"><input type="text" class="form-control" id="text" placeholder="9841131923" name="text" value="{{($result->metaTag)}}"></div>
                                                </div>
                                                <!-- End of metatag -->
                                                <br>
                                                <!-- metabase -->
                                                <div class="row">
                                                    <div class="col-md-3"><label for="text">Metabase:</label></div>
                                                    <div class="col-md-9"><input type="text" class="form-control" id="text" placeholder="email" name="text" value="{{($result->metaDesc)}}"></div>
                                                </div>
                                                <!-- End of metabase -->
                                                <br>
                                            </div>
                                        </div>
                                        <br>
                                </div>
                                <!-- right end -->
                                <br>
                            </div>
                            <br>
                            <br>
                            <button class="btn btn-primary"><b>Update</b></button>
                        </div>
                    </div>
                </div>
            </div>
             {{-- ckeditor//description end --}}
        </form>
    </section>
</section>

@endsection
