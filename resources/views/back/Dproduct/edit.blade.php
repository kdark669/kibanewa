@extends('back.Dproduct.index')
@section('product')
    {{-- overview --}}
    <div class="row">
        <div class="col-lg-12">
        <!-- Title Bar -->
        <ol class="breadcrumb">
            <div class="row">
            <div class="col-md-6">
                <li><i class="fa fa-home"></i>Edit</li>
            </div>
            </div>
        </ol>
        <!-- End of Title Bar -->
        </div>
    </div>
    {{-- overview end--}}

    {{-- edit form start --}}
    <form method="post" action=" {{url('Dproduct/update')}}/{{$result->id}}" enctype="multipart/form-data">
        @csrf
      <div class="row">
          <!-- Slide -->
          <div class="col-lg-12">
            <!-- Upload -->
            <div class="form-wrapper well">
                <div class="form-group">
                    <center>
                        <img src="@if (isset($result->image)) {{url($result->image)}} @endif" class="img img-responsive" height="80" width="80">
                    <center>
                    <input type="hidden" name="pic" value="{{$result->image}}">
                    <input type="file" name="image" class="file">
                    <br>
                    <br>
                    <div class="input-group col-xs-12">
                        <span class="input-group-addon"><i class="fa fa-image"></i></span>
                        <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                        <span class="input-group-btn">
                             <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i> Browse</button>
                        </span>
                    </div>
                    <br>
                </div>
            </div>
            <!-- End of Upload -->

            <!-- Slide Content -->
            <div class="form-wrapper well">
                <div class="form-group">
                    <!-- Title -->
                    <label>Title:</label>
                    <input type="text" class="form-control" placeholder="Title goes here..." name="title" value="{{($result->title)}}">
                    <br>
                    <!-- End of Title -->

                    <!-- Caption -->
                    <label>Heading:</label>
                    <input type="text" class="form-control" placeholder="Caption goes here..." name="heading" value="{{($result->heading)}}">
                    <br>
                    <!-- Caption -->

                    <!-- Keywords -->
                    <label>Description:</label>
                    <input type="text" class="form-control" placeholder="Keywords goes here..." name="description" value="{{($result->description)}}">
                    <br>
                    <!-- End of Keywords -->
                </div>
            </div>
          <!-- End of Slide Content -->

            <!-- Button Bar -->
            <br>
            <div class="row">
                    <div class="col-lg-12">
                      <ol class="breadcrumb">
                          <center>
                          <div class="row">
                              <button   id="my-selector" class="btn btn-primary"><b>Update</b></button>
                          </div>
                          </center>
                      </ol>
                    </div>
            </div>
          <!-- End of Button Bar -->
        </div>
  </form>

@endsection
