@extends('back.Dproduct.index')
@section('product')

<div class="row">
    <div class="col-lg-12">
        <!-- Title Bar -->
        <ol class="breadcrumb">
        <div class="row">
            <div class="col-md-6">
            <li><i class="fa fa-home"></i>Add</li>
            </div>
        </div>
        </ol>
        <!-- End of Title Bar -->
    </div>
</div>

<form method="post" action="{{url('Dproduct/add')}} " enctype="multipart/form-data">
    @csrf
    <div class="row">
         <!-- Slide -->
        <div class="col-lg-12">
            <!-- Upload -->
            <div class="form-wrapper well">
                <div class="form-group">
                    <input type="file" name="image" class="file">
                    <div class="input-group col-xs-12">
                        <span class="input-group-addon"><i class="fa fa-image"></i></span>
                        <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                        <span class="input-group-btn">
                            <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i> Browse</button>
                        </span>
                    </div>
                    <br>
                </div>
            </div>
            <!-- End of Upload -->

            <!-- multimedia Content -->
            <div class="form-wrapper well">
                 <div class="form-group">
                    <!-- heading -->
                    <label>Heading:</label>
                    <input type="text" class="form-control" placeholder="Title goes here..." name="heading" value="">
                    <br>
                     <!-- End of Heading -->

                    <!-- Title -->
                    <label>Title:</label>
                    <input type="text" class="form-control" placeholder="Title goes here..." name="title" value="">
                    <br>
                     <!-- End of Title -->

                    <!-- Caption -->
                    <label>Description:</label>
                    <input type="text" class="form-control" placeholder="Caption goes here..." name="description" value="">
                    <br>
                    <!-- end of Caption -->
                </div>
            </div>
            <!-- End of multimedai Content -->

            <!-- Button Bar -->
            <br>
            <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <center>
                            <div class="row">
                                <button id="my-selector" class="btn btn-primary"><b>Save</b></button>
                            </div>
                            </center>
                        </ol>
                    </div>
            </div>
            <!-- End of Button Bar -->
        </div>
    </div>
</form>

@endsection
