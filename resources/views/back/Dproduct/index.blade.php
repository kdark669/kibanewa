@extends('back.layout.master')
@section('content')
<section id="main-content">
    <section class="wrapper">
        <!-- overstart -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <div class="row">
                    <div class="col-md-6">
                        <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Multimedia</li>
                    </div>
                    <div class="col-md-6">
                            <li class="text-right"><a href="{{url('multimedia')}}"><i class="fa fa-plus"></i>Add</a></li>
                    </div>
                    </div>
                </ol>
            </div>
        </div>
        <!-- end of overstart -->

         <!-- main content -->
    <div class="container">
            <div class="row">
              <div class="col-lg-8">
                    <div class="form-group">
                      <div class="container">
                        <div class="showgrid" id="showgrid">
                            <div id="products" class="row list-group">
                              @foreach($rows as $data)
                                <div class="item col-xs-12 col-lg-4">
                                    <div class="thumbnail">

                                        <img class="group list-group-image" src="@if(isset($data->image)){{url($data->image)}} @endif" alt=""/>
                                        <div class="caption">
                                            <h4 class="group inner list-group-item-heading">{{$data->title}}</h4>
                                        </div>
                                        <div class="caption">
                                            <h4 class="group inner list-group-item-heading">{{$data->heading}}</h4>
                                        </div>
                                        <div class="caption">
                                            <h4 class="group inner list-group-item-heading">{{$data->description}}</h4>
                                        </div>
                                        <div class="row">
                                                <div class="col-xs-12 col-md-12">
                                                <table class="table table-responsive">
                                                    <tr>
                                                        <td style="text-align: center;"><a href="{{url('Dproduct/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                                                        <td style="text-align: center;"><a href="{{url('Dproduct/delete')}}/{{($data->id)}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                                                    </tr>
                                                </table>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                              @endforeach
                            </div>
                        </div>
                      </div>
                    </div>
                    <center>
                      <div class="col-lg-12">
                        {!! $rows->render() !!}
                      </div>
                    </center>
              </div>
              <div class="col-lg-4">
              @yield('product');
              </div>
            </div>
           </div>
    </section>
</section>


@endsection
