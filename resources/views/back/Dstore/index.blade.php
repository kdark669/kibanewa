@extends('back.layout.master')
@section('content')
<section id="main-content">
        <section class="wrapper">
            <!-- overstart -->
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <div class="row">
                        <div class="col-md-6">
                            <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Store</li>
                        </div>
                        <div class="col-md-6">
                            <li class="text-right"><i class="fa fa-plus"></i><a href="{{url('Dstore')}}">Add</a></li>
                        </div>
                        </div>
                    </ol>
                </div>
            </div>
            <!-- overstart end -->

            <div class="row">
                <!--left body: usertable -->
                <div class="col-lg-8">
                    <div class="table-responsive">
                        <table class="table">
                            <!--start heading of the table  -->
                            <thead>
                                <tr>
                                    <th>S.N.</th>
                                    <th>Day</th>
                                    <th>Time</th>
                                    <th colspan="3" style="text-align: center;">Setting</th>
                                </tr>
                            </thead>
                            <!-- end of table heading -->
                            <!-- table body start -->
                            <tbody>
                                @foreach ($rows as $data)
                                    <tr>
                                        <th >{{$loop->iteration}}</th>
                                       <td>{{$data->day}}</td>
                                       <td>{{$data->time}}</td>

                                       <td style="text-align: center;"><a href="{{url('Dstore/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                                       <td style="text-align: center;"><a href="{{url('Dstore/delete')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- bottom pagination -->
                <center>
                        <div class="col-lg-12">
                            {!! $rows->render() !!}
                        </div>
                    </center>
                    <!-- end of bottom pagination -->
                </div>
                <!--rightbody: add user start -->
                <div class="col-lg-4">
                        @yield('store')
                 </div>
                <!-- end of rightbody -->
            </div>
        </section>
</section>
@endsection
