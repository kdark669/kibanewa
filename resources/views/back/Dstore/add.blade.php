@extends('back.Dstore.index')
@section('store')

{{-- navigation --}}
<div class="row">
        <div class="col-lg-12">
          <!-- Title Bar -->
          <ol class="breadcrumb">
            <div class="row">
              <div class="col-md-6">
                <li><i class="fa fa-home"></i>Add</li>
              </div>
            </div>
          </ol>
          <!-- End of Title Bar -->
        </div>
</div>
{{-- end of navigation --}}

<form method="post" action="{{url('Dstore/add')}}" enctype="multipart/form-data">
    @csrf
    <div class="row">
        {{-- store content --}}
        <div class="form-wrapper well">
            <div class="form-group">
                <!-- Title -->
                <label>Title:</label>
                <input type="text" class="form-control" placeholder="Title goes here..." name="title" value="">
                <br>
                <!-- End of Title -->

                <!-- Caption -->
                <label>Heading:</label>
                <input type="text" class="form-control" placeholder="Heading goes here..." name="heading" value="">
                <br>
                <!-- Caption -->

                <label>Day:</label>
                <input type="text" class="form-control" placeholder="day" name="day">
                <br>

                <label>Time:</label>
                <input type="text" class="form-control" placeholder="repassword" name="time">
                <br>

                <label>Description:</label>
                <input type="text" class="form-control" placeholder="repassword" name="description">
                <br>

                <label>address:</label>
                <input type="text" class="form-control" placeholder="repassword" name="address">
                <br>

                <label>address2:</label>
                <input type="text" class="form-control" placeholder="repassword" name="address2">
                <br>

                <label>Contact:</label>
                <input type="text" class="form-control" placeholder="repassword" name="contact">
                <br>
            </div>
            <!-- End of user Content -->
            <!-- Button Bar -->
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                    <center>
                    <div class="row">
                        <button id="my-selector" class="btn btn-primary"><b>Save</b></button>
                    </div>
                    </center>
                    </ol>
                </div>
            </div>
        <!-- End of Button Bar -->
        </div>
        {{-- store content end --}}

    </div>

@endsection
