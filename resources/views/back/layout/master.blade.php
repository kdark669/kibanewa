<!DOCTYPE html>
<html>
<head>
	@include('back.common.header')
</head>
<body>
	@include('back.common.navbar')
	@yield('content')
	@include('back.common.footer')
</body>
</html>