@extends('back.layout.master')
@section('content')
    <!--main content start-->
    <section id="main-content">
            <section class="wrapper">
              <!--overview start-->
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="page-header"><i class="fa fa-laptop"></i>Home</h3>
                  <ol class="breadcrumb">
                  <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a></li>
                    <!-- <li><i class="fa fa-laptop"></i>Dashboard</li> -->
                  </ol>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <a href="{{url('admin')}}">
                    <div class="info-box blue-bg">
                      <i class="fa fa-home"></i>
                      <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Home </h4>
                    </div>

                  <!--/.info-box-->
                </div>
                <!--/.col-->
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <a href="{{url('Dabout')}}">
                    <div class="info-box brown-bg">
                      <i class="fa fa-files-o" aria-hidden="true"></i>
                      <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> About </h4>
                    </div>
                  </a>
                  <!--/.info-box-->
                </div>
                <!--/.col-->
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <a href="{{url('Dstore')}}">
                    <div class="info-box dark-bg">
                      <i class="fa fa-bullhorn" aria-hidden="true"></i>
                      <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;">Store</h4>
                    </div>
                  </a>
                  <!--/.info-box-->
                </div>
                <!--/.col-->
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <a href="{{url('Dproduct')}}">
                    <div class="info-box green-bg">
                      <i class="fa fa-film" aria-hidden="true"></i>
                      <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Products </h4>
                    </div>
                  </a>
                  <!--/.info-box-->
                </div>
                <!--/.col-->
              </div>

              <div class="row">
                <!--/.col-->
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <a href="{{url('user')}}">
                    <div class="info-box brown-bg">
                      <i class="fa fa-user" aria-hidden="true"></i>
                      <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Users </h4>
                    </div>
                  </a>
                  <!--/.info-box-->
                </div>
                <!--/.col-->
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <a href="{{url('setting')}}">
                    <div class="info-box dark-bg">
                      <i class="fa fa-cog" aria-hidden="true"></i>
                      <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Settings </h4>
                    </div>
                  </a>
                  <!--/.info-box-->
                </div>

              </div>
            </section>
          </section>
	<!--main content end-->


@endsection
