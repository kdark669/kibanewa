@extends('back.layout.master')
@section('content')
{{-- setting body --}}
<section id="main-content">
    <section class="wrapper">
            <form method="POST" action="{{url('setting/update')}}/{{$result->id}}" enctype="multipart/form-data">
                @csrf
                <!-- overstart -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <div class="row">
                                <div class="col-md-6">
                                    <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Setting</li>
                                </div>
                            </div>
                        </ol>
                    </div>
                </div>
                {{-- end of overview --}}

                <!--formcontent -->
                <div class="row">
                        <!-- left -->
                        <div class="col-lg-8">
                            <h2>Information</h2>
                            <div class="form-wrapper well">
                                <div class="form-group">
                                    <!-- Company -->
                                    <div class="row">
                                        <div class="col-md-3"><label for="text">Company:</label></div>
                                        <div class="col-md-9"><input type="text" class="form-control" id="text" placeholder="KITES NEPAL " name="company" value="{{($result->company)}}"></div>
                                    </div>
                                    <!-- End of Company -->
                                    <br>
                                    <!--description-->
                                    <div class="row">
                                        <div class="col-md-3"><label for="text">description:</label></div>
                                        <div class="col-md-9"><input type="text" class="form-control" id="text" placeholder="Gapali -4, Suryavinyak" name="description" value="{{($result->description)}}"></div>
                                    </div>
                                    <!-- end of description -->
                                    <br>
                                    <!--Adress -->
                                    <div class="row">
                                        <div class="col-md-3"><label for="text">Address:</label></div>
                                        <div class="col-md-9"><input type="text" class="form-control" id="text" placeholder="Gapali -4, Suryavinyak" name="address" value="{{($result->address)}}"></div>
                                    </div>
                                    <!-- end of address -->
                                    <br>
                                    <!-- contact -->
                                    <div class="row">
                                        <div class="col-md-3"><label for="text">Contact:</label></div>
                                        <div class="col-md-9"><input type="text" class="form-control" id="text" placeholder="9841131923" name="contact" value="{{($result->contact)}}"></div>
                                    </div>
                                    <!-- End of Contact -->
                                    <br>
                                    <!-- email -->
                                    <div class="row">
                                        <div class="col-md-3"><label for="email">Email</label></div>
                                        <div class="col-md-9"><input type="email" class="form-control" id="text" placeholder="email" name="email" value="{{($result->email)}}"></div>
                                    </div>
                                    <!-- End of email -->
                                    <br>
                                </div>
                            </div>
                            <br>
                            <br>
                            <h2>Social</h2>
                            <div class="form-wrapper well">
                                <div class="form-group">
                                    <!-- Company -->
                                    <div class="row">
                                        <div class="col-md-3"><label for="text">Facebook:</label></div>
                                        <div class="col-md-9"><input type="text" class="form-control" id="text" placeholder="Kitesnepal" name="facebook" value="{{($result->facebook)}}"></div>
                                    </div>
                                    <!-- End of Company -->
                                    <br>
                                </div>
                            </div>
                        </div>
                        <!-- end of left -->

                        <!-- right -->
                        <div class="col-lg-4">
                            <h2>SEO</h2>
                            <div class="form-wrapper well">
                                <div class="form-group">
                                    <!-- Caption -->
                                    <div class="row">
                                        <div class="col-md-3"><label for="text">Caption:</label></div>
                                        <div class="col-md-9"><input type="text" class="form-control" id="text" placeholder="KIBA NEWA" name="text" value="{{($result->caption)}}"></div>
                                    </div>
                                    <!-- End of caption -->
                                    <br>
                                    <!--keywords -->
                                    <div class="row">
                                        <div class="col-md-3"><label for="text">keywords:</label></div>
                                        <div class="col-md-9"><input type="text" class="form-control" id="text"  name="text" value="{{($result->keywords)}}"></div>
                                    </div>
                                    <!-- end of keywords -->
                                    <br>
                                    <!-- metatag -->
                                    <div class="row">
                                        <div class="col-md-3"><label for="text">Metatag:</label></div>
                                        <div class="col-md-9"><input type="text" class="form-control" id="text" name="text" value="{{($result->metaTag)}}"></div>
                                    </div>
                                    <!-- End of metatag -->
                                    <br>
                                    <!-- metabase -->
                                    <div class="row">
                                        <div class="col-md-3"><label for="text">Metabase:</label></div>
                                        <div class="col-md-9"><input type="text" class="form-control" id="text" name="text" value="{{($result->metaDesc)}}"></div>
                                    </div>
                                    <!-- End of metabase -->
                                    <br>
                                </div>
                            </div>
                            <br>
                            <br>
                            <h2>Logo</h2>
                            <div class="form-wrapper well">
                                <div class="form-group">
                                    <!-- logo -->
                                    <br>
                                    <center>
                                        <img src="@if(isset($result->logo)) {{url($result->logo)}} @endif" class="img img-responsive" alt="images">
                                    </center>
                                    <br>
                                    <input type="hidden" name="pic" value="{{($result->logo)}}">
                                    <!-- end of logo -->
                                    <!-- button -->
                                    <input type="file" name="logo" class="file">
                                    <div class="input-group col-xs-12">
                                        <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                        <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                                        <span class="input-group-btn">
                                            <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i> Browse</button>
                                        </span>
                                    </div>
                                    <!-- End of button -->
                                    <!--Adress -->
                                    <br>
                                </div>
                            </div>
                        </div>
                        <!-- right end -->
                </div>
                <!-- formcontent end -->

                <!--published  -->
                <br>
                <div class="row">
                    <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <center>
                            <button class="btn btn-default">Update</button>
                        </center>
                    </ol>
                    </div>
                </div>

             </form>
    </section>
</section>
{{-- setting body end --}}

@endsection
