@extends('back.user.index')
@section('user')

<!-- overview -->
<div class="row">
    <div class="col-lg-12">
        <!-- Title Bar -->
        <ol class="breadcrumb">
            <div class="row">
            <div class="col-md-6">
                <li><i class="fa fa-home"></i>Edit User</li>
            </div>
            </div>
        </ol>
        <!-- End of Title Bar -->
    </div>
</div>
<!-- end of overview -->

{{-- user's edit form --}}
<form method="post" action="{{url('user/update')}}/{{$result->id}}" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <!-- Slide -->
        <div class="col-lg-12">
            <!-- Upload -->
            <div class="form-wrapper well">
                <div class="form-group">
                    <center>
                        <img src="@if (isset($result->image)) {{url($result->image)}} @endif" class="img img-responsive" height="80" width="80">
                    <center>
                <br>
                <!-- for the name of the image -->
                <input type="hidden" name="pic" value="{{$result->image}}">
                <input type="file" name="image" class="file">
                <!-- end for file name -->

                <!-- button -->
                <div class="input-group col-xs-12">
                <span class="input-group-addon"><i class="fa fa-image"></i></span>
                    <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                    <span class="input-group-btn">
                    <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i> Browse</button>
                </span>
                </div>
                <!-- endfor button-->
                <br>
            </div>
        </div>
        <!-- End of Upload -->

        <!-- user detail -->
        <div class="form-wrapper well">
            <div class="form-group">
                <!-- name -->
                <label>Name:</label>
                <input type="text" class="form-control" placeholder="Title goes here..." name="name" value="{{($result->name)}}">
                <br>
                <!-- End of name -->

                <!-- Caption -->
                <label>Email:</label>
                <input type="text" class="form-control" placeholder="Caption goes here..." name="email" value="{{($result->email)}}">
                <br>
                <!-- Caption -->
            </div>
        </div>
        <!-- End of userdetail -->
    </div>

    <!-- Button Bar -->
    <br>
    <div class="row">
        <div class="col-lg-12">
            <center>
                <ol class="breadcrumb">
                  <div class="row">
                      <button id="my-selector" class="btn btn-primary"><b>Update</b></button>
                  </div>
              </ol>
            </center>
        </div>
    </div>
    <!-- End of Button Bar -->
</form>
{{-- end of user's edit form --}}

@endsection
