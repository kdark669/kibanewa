@extends('back.user.index')
@section('user')
{{-- overview --}}
<div class="row">
<div class="col-lg-12">
    <!-- Title Bar -->
    <ol class="breadcrumb">
    <div class="row">
        <div class="col-md-6">
        <li><i class="fa fa-home"></i>Add User</li>
        </div>
    </div>
    </ol>
    <!-- End of Title Bar -->
</div>
</div>
{{-- end of overview --}}
{{-- adding user's form --}}
<form method="post" action="{{url('user/add')}}" enctype="multipart/form-data">
@csrf
<div class="row">
        <!-- Slide -->
    <div class="col-lg-12">
        <!-- Upload -->
        <div class="form-wrapper well">
            <div class="form-group">
            <input type="file" name="image" class="file">
            <div class="input-group col-xs-12">
                <span class="input-group-addon"><i class="fa fa-image"></i></span>
                    <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                    <span class="input-group-btn">
                    <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i> Browse</button>
                </span>
            </div>
            <br>
            </div>
        </div>
        @if (count($errors)>0)
        <label>
            <ul>
                @foreach ($errors->get('image') as $data)
                <li class="alert alert-danger">{{$data}}
                </li>
                @endforeach
            </ul>
        </label>
        @endif
        <br>
        <!-- End of Upload -->

        <!-- user content -->
        <div class="form-wrapper well">
            <div class="form-group">
            <!-- Name -->
            <label>Name:</label>
            <input type="text" class="form-control" placeholder="Name goes here..." name="name" value="">
            <br>
            @if (count($errors)>0)
                <label>
                    <ul>
                        @foreach ($errors->get('name') as $data)
                        <li class="alert alert-danger">{{$data}}
                        </li>
                        @endforeach
                    </ul>
                </label>
            @endif
            <br>

            <!-- End of Name -->

            <!-- email -->
            <label>Email:</label>
            <input type="text" class="form-control" placeholder="Email goes here..." name="email" value="">
            <br>
            @if (count($errors)>0)
                <label>
                    <ul>
                        @foreach ($errors->get('email') as $data)
                        <li class="alert alert-danger">{{$data}}
                        </li>
                        @endforeach
                    </ul>
                </label>
            @endif
            <br>
            <!-- email -->

            <label>Password:</label>
            <input type="text" class="form-control" placeholder="password" name="password">
            <br>
            @if (count($errors)>0)
            <label>
                <ul>
                    @foreach ($errors->get('password') as $data)
                    <li class="alert alert-danger">{{$data}}
                    </li>
                    @endforeach
                </ul>
            </label>
            @endif
            <br>

            <label>RePassword:</label>
            <input type="text" class="form-control" placeholder="repassword" name="repassword">
            <br>
            @if (count($errors)>0)
            <label>
                <ul>
                    @foreach ($errors->get('repassword') as $data)
                    <li class="alert alert-danger">{{$data}}
                    </li>
                    @endforeach
                </ul>
            </label>
            @endif
            <br>
            </div>
            <!-- End of user Content -->
            <!-- Button Bar -->
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                    <center>
                    <div class="row">
                        <button id="my-selector" class="btn btn-primary"><b>Save</b></button>
                    </div>
                    </center>
                    </ol>
                </div>
            </div>
            <!-- End of Button Bar -->
        </div>
    </div>
</div>
</form>


@endsection
