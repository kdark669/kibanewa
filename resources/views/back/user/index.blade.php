@extends('back.layout.master')
@section('content')
<section id="main-content">
        <section class="wrapper">
             <!-- overstart -->
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <div class="row">
                        <div class="col-md-6">
                            <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Users</li>
                        </div>
                        <div class="col-md-6">
                            <li class="text-right"><i class="fa fa-plus"></i><a href="{{url('user')}}">Add</a></li>
                        </div>
                        </div>
                    </ol>
                </div>
            </div>
            <!-- overstart end -->

            <!-- main content -->
            <!-- start of user table -->
            <div class="row">
                <!--left body: usertable -->
                <div class="col-lg-8">
                    <div class="table-responsive">
                        <table class="table">
                            <!--start heading of the table  -->
                            <thead>
                                <tr>
                                    <th>S.N.</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th colspan="3" style="text-align: center;">Setting</th>
                                </tr>
                            </thead>
                            <!-- end of table heading -->
                            <!-- table body start -->
                            <tbody>
                                @foreach ($rows as $data)
                                <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                   <td><img src="@if(isset($data->image)) {{url($data->image)}} @endif" class="img img-responsive" height="80" width="80"></td>
                                   <td>{{($data->name)}}</td>
                                   <td>{{($data->email)}}</td>
                                   <td style="text-align: center;"><a href="{{('user/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                                    <td style="text-align: center;"><a href="{{('user/delete')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                            <!-- end of table body -->
                        </table>
                    </div>
                    <!-- bottom pagination -->
                    <center>
                        <div class="col-lg-12">
                           {!!$rows->render() !!}
                        </div>
                    </center>
                    <!-- end of bottom pagination -->
                </div>
                <!-- leftbody : usertable end -->

                <!--rightbody: add user start -->
                <div class="col-lg-4">
                @yield('user')
                </div>
                <!-- end of rightbody -->
            </div>
             <!-- end of user table -->
        </section>
</section>

@endsection
