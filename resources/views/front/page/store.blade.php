@extends('front.layout.master')
@section('content')

{{-- store start --}}
<section class="page-section cta">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <div class="cta-inner text-center rounded">
                    <h2 class="section-heading mb-5">
                        <span class="section-heading-upper">Come On In</span>
                        <span class="section-heading-lower">We're Open</span>
                    </h2>
                    <ul class="list-unstyled list-hours mb-5 text-left mx-auto">
                        <li class="list-unstyled-item list-hours-item d-flex">
                        Sunday
                        <span class="ml-auto">7:00 AM to 8:00 PM</span>
                        </li>
                        <li class="list-unstyled-item list-hours-item d-flex">
                        Monday
                        <span class="ml-auto">7:00 AM to 8:00 PM</span>
                        </li>
                        <li class="list-unstyled-item list-hours-item d-flex">
                        Tuesday
                        <span class="ml-auto">7:00 AM to 8:00 PM</span>
                        </li>
                        <li class="list-unstyled-item list-hours-item d-flex">
                        Wednesday
                        <span class="ml-auto">7:00 AM to 8:00 PM</span>
                        </li>
                        <li class="list-unstyled-item list-hours-item d-flex">
                        Thursday
                        <span class="ml-auto">7:00 AM to 8:00 PM</span>
                        </li>
                        <li class="list-unstyled-item list-hours-item d-flex">
                        Friday
                        <span class="ml-auto">7:00 AM to 8:00 PM</span>
                        </li>
                        <li class="list-unstyled-item list-hours-item d-flex">
                        Saturday
                        <span class="ml-auto">9:00 AM to 8:00 PM</span>
                        </li>
                        <span>(live music starting @ 7:00 PM onward)</span>
                    </ul>
                    <p class="address mb-5">
                        <em>
                        <strong>Yachhen</strong>
                        <br>
                        Bhaktapur, Nepal
                        </em>
                    </p>
                    <p class="mb-0">
                        <small>
                        <em>Call Anytime</em>
                        </small>
                        <br>
                        (317) 585-8468
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="map">
    <div id="googleMap" style="width:100%;height:400px;"></div>

    <script>
    function myMap() {
    var mapProp= {
    center:new google.maps.LatLng(27.673481,85.432425),
    zoom:5,
    };
    var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
    }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAA2v1mlKzP2f4Mpi-Nm4UMRdAbcMhFhS0
    ">
    </script>
</section>
{{-- store end --}}

@endsection
