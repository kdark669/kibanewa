{{-- footer --}}
<footer class="footer text-faded text-center py-5">
        <div class="container">
            <p class="m-0 small">Copyright &copy; KibaNewa Restaurant 2018</p>

        </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="{{url('frontend/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{url('frontend/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
