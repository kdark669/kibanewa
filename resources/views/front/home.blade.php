@extends('front.layout.master')
@section('content')


    <!-- description -->
    <section class="page-section clearfix">
        <div class="container">
            <div class="intro">
            <img class="intro-img img-fluid mb-3 mb-lg-0 rounded" src="{{url('frontend/img/intro.jpg')}}" alt="">
            <div class="intro-text left-0 text-center bg-faded p-5 rounded">
                <h2 class="section-heading mb-4">
                <span class="section-heading-upper">Fresh Food</span>
                <span class="section-heading-lower">Happy Faces</span>
                </h2>
                <p class="mb-3">A fresh food can light up the life and the day.Tripically food can engeries human body ane the taste the relax the mind.
                </p>
                <div class="intro-button mx-auto">
                <a class="btn btn-primary btn-xl" href="store.html">Visit Us Today!</a>
                </div>
            </div>
            </div>
        </div>
    </section>
    <!--end of description-->

    <!-- promise -->
    <section class="page-section cta">
    <div class="container">
        <div class="row">
        <div class="col-xl-9 mx-auto">
            <div class="cta-inner text-center rounded">
            <h2 class="section-heading mb-4">
                <span class="section-heading-upper">Our Promise</span>
                <span class="section-heading-lower">To You</span>
            </h2>
            <p class="mb-0">We are dedicated to providing you with friendly service, a welcoming atmosphere, and above all else, excellent products made with the highest quality ingredients and the traditional taste. If you are not satisfied, please let us know and we will do whatever we can to make things right! Our Promise to you is to make ur fooding time happy with the declious taste.</p>
            </div>
        </div>
        </div>
    </div>
    </section>
    <!-- end of promise body -->
@endsection
