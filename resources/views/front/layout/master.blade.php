<!DOCTYPE html>
<html lang="en">
<head>
    @include('front.common.header')
</head>
<body>
    {{-- navbar start --}}
    @include('front.common.navbar')
    {{-- navbar end --}}

    @yield('content')
    {{-- footer --}}
    @include('front.common.footer')
    {{-- end of footer --}}
</body>
</html>
