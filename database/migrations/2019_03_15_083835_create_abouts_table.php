<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable;
            $table->string('heading');
            $table->string('title');
            $table->longtext('description')->nullable();
            $table->string('caption')->nullable();
            $table->string('keywords')->nullable();
            $table->string('metaTag')->nullable();
            $table->string('metaDesc')->nullable();
            $table->integer('UID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}
