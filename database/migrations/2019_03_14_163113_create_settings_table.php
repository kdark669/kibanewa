<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company');
            $table->string('logo');
            $table->string('description');
            $table->string('contact');
            $table->string('address');
            $table->string('email');
            $table->string('facebook');
            $table->string('caption')->nullable();
            $table->string('keyword')->nullable();
            $table->string('metaTag')->nullable();
            $table->string('metaDesc')->nullable();
            $table->integer('UID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
